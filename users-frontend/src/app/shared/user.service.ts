import { Injectable } from '@angular/core';
//import {User} from './user.model';
import {HttpClient} from '@angular/common/http'
import {FormGroup, FormControl, Validators} from '@angular/forms'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  //formData: User
  readonly rootUrl = 'https://localhost:44308/api';
  constructor(private http:HttpClient) { }
  list : {userID: number, userName: string, user: string, email: string, phone:string }[];
  form: FormGroup =  new FormGroup({
    userID: new FormControl(0),
    userName: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', Validators.minLength(8))
  });

  initializeFormGroup() {
    this.form.setValue({
      userID: 0,
      userName: '',
      name: '',
      email: '',
      phone: ''
    });
  }

  postUser() {
    debugger
    return this.http.post(this.rootUrl + '/users', this.form.value);
  }

  putUser() {
    debugger
    let id = this.form.value.userID;
    return this.http.put(this.rootUrl + '/users/'+ id, this.form.value);
  }

  deleteUser(id) {
    return this.http.delete(this.rootUrl + '/users/'+ id);
  }

  resfreshListUsers(){
    return this.http.get(this.rootUrl + '/users').toPromise()
    .then(res => {
      debugger 
      this.list = res as {userID: number, userName: string, user: string, email: string, phone:string }[]
    });
  }

  populateForm(user){
    this.form.setValue(user);

  }
}
