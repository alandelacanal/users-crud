import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../shared/user.service';

import { MatDialog } from '@angular/material/dialog';
import { MatDialogConfig } from '@angular/material/dialog';
import { UserComponent } from '../user/user.component';
import { NotificationService } from '../../shared/notification.service';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(public service: UserService,public notificationService: NotificationService, private dialog: MatDialog) { }

  displayedColumns: string[] = ['userName', 'name', 'email', 'phone', 'actions'];
 
  ngOnInit(): void {    
    this.service.resfreshListUsers();  
  }

  onCreate(){
    this.service.initializeFormGroup();
    const dialogConfig =  new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "40%";
    this.dialog.open(UserComponent,dialogConfig);
  }

  onEdit(row){
    this.service.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "40%";
    this.dialog.open(UserComponent,dialogConfig);
    
  }

  onDelete($key){
    if(confirm('Are you sure to delete this record ?')){
    this.service.deleteUser($key).subscribe(
        res => {
          this.service.resfreshListUsers();
          this.notificationService.warn('! Deleted successfully');
        },
        err => {
          console.log(err)
        }
      );
    }
  }

}
