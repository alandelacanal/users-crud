import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { NotificationService } from '../../shared/notification.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(public service: UserService, 
    public notificationService: NotificationService,
    public dialogRef: MatDialogRef<UserComponent>) { }

  ngOnInit(): void {
    
  }

  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
  }

  onSubmit() {    
    if (this.service.form.value.userID != 0)
      this.service.putUser().subscribe(
        res => {
          this.service.form.reset();
          this.service.initializeFormGroup();
          this.notificationService.success('Submitted successfully');
          this.service.resfreshListUsers();
          this.onClose();
  
        },
        err => {
          console.log(err)
        }
      );
    else
    this.service.postUser().subscribe(
      res => {
        this.service.form.reset();
        this.service.initializeFormGroup();
        this.notificationService.success('Submitted successfully');
        this.service.resfreshListUsers();
        this.onClose();

      },
      err => {
        console.log(err)
      }
    );
  }

  onClose(){
    this.service.form.reset();
    this.service.initializeFormGroup();
    this.dialogRef.close();
  }

}
